# Trabalho 01 - Fork Wait

Escreva um trecho de código que utiliza a função fork() e gera uma árvore de busca
binária (ou quase isso) com seu primeiro nome. Para tal siga as seguintes regras:

1. Primeira letra será a raiz da árvore
2. Cada letra seguinte será inserida a direita, se a letra for maior que a raiz, ou a
esquerda, se a letra for menor que a raiz.
3. Esse procedimento (verificar a raiz e inserir a direita ou a esquerda) deve ser
realizado recursivamente.

Ex: dalcimar

![Tree Example](/Sistemas Operacionais/assets/t01/tree_example.png "Tree Example")

Atenção, um processo deve mostrar uma mensagem se identificando (“proc-A”... “proc-I”)
* quando ele acaba de ser criado
* e quando ele está prestes a morrer
* cada processo gerado deve imprimir o seu PID e o PPID
* você deve garantir que um pai sempre morre depois de seu filho!

Para a árvore acima a saída poderá ser:

![Out Example](/Sistemas Operacionais/assets/t01/out_example.png "Out Example")

## Considerações sobre o trabalho
Com base no exemplo, a árvore do meu nome fica assim:

![My Name's Tree](/Sistemas Operacionais/assets/t01/tree.png "My Name's Tree")

Adicionei os estados de 'mypid' e 'index' para facilitar o entendimento do código.