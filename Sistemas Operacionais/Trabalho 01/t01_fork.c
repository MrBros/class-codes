//gcc t01_fork.c -o t01_fork && ./t01_fork

#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

char name[5] = "MARIO";
int index = 0;
__pid_t mypid;

void process_status(int born_flag) {
    if(born_flag) 
        printf("process %c, PID %ld, Parent PID %ld, has been created.\n", name[index], (long int)getpid(), (long int)getppid());
    else
        printf("process %c, PID %ld finished.\n", name[index], (long int)getpid());   
}

void main() {
    // process M
    process_status(1);

    mypid = fork();
    if(mypid == 0) {
        index = 1;
        // process A
        process_status(1);
        
        mypid = fork();
        if(mypid == 0) {
            index = 3;
            // process I
            process_status(1);
        }
    }
    else if(mypid > 0) {
        mypid = fork();
        if(mypid == 0) {
            index = 2;
            // process R
            process_status(1);
            
            mypid = fork();
            if(mypid == 0) { 
                index = 4;
                // process O
                process_status(1);
            }
        }
        wait(NULL);
    }
    wait(NULL);
    
    process_status(0);
}
