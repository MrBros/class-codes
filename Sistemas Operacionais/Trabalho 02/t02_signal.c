// gcc t02_signal.c -o t02_signal && ./t02_signal

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define ever ;;

int SIGINT_counter = 0;
char decision;

void SIGINT_handler() {
	printf("\n");
	if(SIGINT_counter < 10)
		SIGINT_counter++;
}

void SIGTSTP_handler() {
	printf("\nNúmero de sinais 'SIGINT': %d\n", SIGINT_counter);
}

void SIGALRM_handler() {
	if(decision == '\0') 
		exit(0); 
}

int main() {
	signal(SIGINT, SIGINT_handler);
	signal(SIGTSTP, SIGTSTP_handler);
	signal(SIGALRM, SIGALRM_handler);

	for(ever) {
		if(SIGINT_counter == 10) {
			decision = '\0';
			printf("\nReally exit(Y/n)? ");
			alarm(5);
			scanf(" %c", &decision);

			if(decision == 'Y' || decision == 'y')
				kill(getpid(), SIGKILL);
			else if(decision == 'N' || decision == 'n') 
				SIGINT_counter = 0;
		}
	}
}