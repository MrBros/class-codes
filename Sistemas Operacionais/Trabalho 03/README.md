# Trabalho 03 - Threads
Escolha duas séries quaisquer para aproximar o número pi do site Pi series 1 ou Pi series
2 ou outro site qualquer. Veja exemplos sequenciais em Pi sequencial. Implemente a
versão paralela dessas séries, utilizando pthreads, sequindo os seguintes requisitos:
* Devem ser calculados pelo menos 1 bilhão (10^9) de termos de cada série.
Use variáveis reais de dupla precisão (double) nos cálculos;
O programa deve dividir o espaço de cálculo uniformemente entre as N threads
e.x. 1 bilhão de termos com 2 threads = 500 milhões de termos em cada
thread;
* cada thread efetua uma soma parcial de forma autônoma;
* Para evitar o uso de mecanismos de sincronização, cada thread T[i] deve
depositar seu resultado parcial na posição result[i] de um vetor de resultados
parciais;
* Após o término das threads de cálculo, o programa principal soma os resultados
parciais ob dos por elas e apresenta o resultado final na tela;
* Execute as threads no seu computador pessoal;
* Execute as soluções com N = {1, 2, 4, 8 e 16} threads
* Marque o tempo necessário para calcular Pi para cada N e faça um gráfico de
linhas (NxTempo) apresentado os resultados obtidos;
* Compare o resultado das duas soluções (series) escolhidas, indicando qual série é
mais eficiente em termos de tempo e qualidade da solução (i.e. valor mais exato
de pi).

## Considerações sobre o trabalho
Para executar o programa é necessário passar 3 argumentos:

```time ./t03_threads [SERIE] [NUM_THREADS] [NUM_TERMS]```

Onde:
* [SERIE]: Qual série será calculada [wallis,leibniz];
* [NUM_THREADS]: Número de threads que calcularão os termos das séries;
* [NUM_TERMS]: Número de termos que serão calculados.

O arquivo script.sh foi criado para automatizar a compilação e execução do programa para ambas as séries, (1,2,4,8,16) threads e 1 bilhão de termos. Arquivos '.csv' são criados pelo script para armazenar a saída do programa, bem como o tempo de execução.

O arquivo 'relatorio.pdf' contém uma análise dos resultados encontrados.