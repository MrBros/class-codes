#!/bin/bash

# Compilando o programa
gcc t03_threads.c -o t03_threads -lpthread -lm

# Executanto o programa para todos os parametros
for parameter in "wallis" "leibniz"; do
    echo "num_threads,pi,time(s)" > data_${parameter}.csv
    for num_threads in 1 2 4 8 16; do
        /usr/bin/time -f "%e" -o /tmp/time.txt ./t03_threads $parameter $num_threads 1000000000 >> data_${parameter}.csv
        cat /tmp/time.txt >> data_${parameter}.csv
    done
done
    
