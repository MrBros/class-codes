// gcc t03_threads.c -o t03_threads -lpthread -lm

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

int NUM_THREADS, NUM_TERMS;
double *result;
double pi = 0;

void* leibniz_series(void* id) {
    long tid = (long)id;
    long terms = NUM_TERMS/NUM_THREADS;
    long start = tid*terms;
    long end = start + terms;
    float s = 1;

    for(long i = start; i < end; i++) {
        result[8*tid] += (s / (2*i + 1)); 
        s = -s;
    }
}

void* wallis_series(void* id) {
    long tid = (long)id;
    long terms = NUM_TERMS/NUM_THREADS;
    long start = tid*terms;
    long end = start + terms;
    if(start == 0) start = 1;
    
    for(long i = start; i < end; i++)    
        result[8*tid] *= (4.0*i*i) / (4.0*i*i - 1.0);
}

int main(int argc, char* argv[]) {
    void *function;

    if(argc == 4) {
        if(!strcmp(argv[1],"leibniz"))
            function = leibniz_series;
        else if(!strcmp(argv[1],"wallis"))
            function = wallis_series;
        else {
            perror("invalid_command");
            exit(-1);
        }
        
        NUM_THREADS = atoi(argv[2]);
        NUM_TERMS = atol(argv[3]);
        result = malloc(sizeof(double)*8*NUM_THREADS);
    }
    else {
        perror("invalid_argumentation");
        exit(-1);
    }

    pthread_t threads[NUM_THREADS];
    long status;
    
    if(!strcmp(argv[1],"leibniz"))
        for(int i = 0; i < NUM_THREADS; i++)
            result[8*i] = 0;
    else if(!strcmp(argv[1],"wallis"))
        for(int i = 0; i < NUM_THREADS; i++)
            result[8*i] = 1;

    for(int i = 0; i < NUM_THREADS; i++) {
        status = pthread_create(&threads[i], NULL, function, (void*)(intptr_t)i);

        if(status) {
            perror("pthread_create");
            free(result);
            exit(-1);
        }
    }

    if (function == leibniz_series) {
        for(int i = 0; i < NUM_THREADS; i++) {
            pthread_join(threads[i], NULL);
            pi += result[8*i];
        }
        pi *= 4;
    }
    else if(function == wallis_series)
    {
        pi = 2;
        for(int i = 0; i < NUM_THREADS; i++)
        {
            pthread_join(threads[i], NULL);
            pi *= result[8*i]; 
        }
    }
    
    printf("%i,", NUM_THREADS);
    printf("%1.16lf,", pi);

    free(result);
    pthread_exit(0);
}