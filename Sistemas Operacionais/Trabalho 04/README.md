# Trabalho 04 - Grafo de Precedência
Suponha o grafo de precedência abaixo com 5 processos.

![Graph](/Sistemas Operacionais/assets/t04/graph.png "Graph")

Adicione semáforos a esses processos de modo que a precedência definida acima seja
alcançada
Ao iniciar sua execução o processo imprime na tela uma mensagem (e.g. ‘Iniciando A’) e
espera um tempo aleatório entre 1 e 5 segundos para finalizar.
Ao finalizar o processo imprime uma mensagem (e.g. ‘Finalizando processo ‘A’)

## Considerações sobre o trabalho
O entendimento do trabalho fica mais fácil se entender os processos como vértices do grafo e os semáforos como arestas.     