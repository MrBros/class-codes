// gcc t04_graph.c -o t04_graph -lpthread && ./t04_graph    

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>
#include <unistd.h>

#define NUM_THREADS 5

sem_t semC1, semC2, semD, semE;

// Definindo uma função para cada processo
void* procA(void* id) {
    // O processo A executa assim que a thread é criada
    int seg = 1 + (rand()%4);
    printf("Iniciando A: %ds\n", seg);
    sleep(seg);
    printf("Finalizando processo A\n");
    
    sem_post(&semC1); // Libera semC1 para executar
}

void* procB(void* id) {
    // O processo B executa assim que a thread é criada
    int seg = 1 + (rand()%4);
    printf("Iniciando B: %ds\n", seg);
    sleep(seg);
    printf("Finalizando processo B\n");
    
    sem_post(&semC2); // Libera semC2 para executar
}

void* procC(void* id) {
    sem_wait(&semC1); // Espera semC1 ser liberado para executar por procA
    sem_wait(&semC2); // Espera semC2 ser liberado para executar por procB
    
    int seg = 1 + (rand()%4);
    printf("Iniciando C: %ds\n", seg);
    sleep(seg);
    printf("Finalizando processo C\n");
    
    sem_post(&semD); // Libera semD para executar
    sem_post(&semE); // Libera semE para executar
}

void* procD(void* id) {
    sem_wait(&semD); // Espera semD ser liberado para executar por procD
    
    int seg = 1 + (rand()%4);
    printf("Iniciando D: %ds\n", seg);
    sleep(seg);
    printf("Finalizando processo D\n");
}

void* procE(void* id) {
    sem_wait(&semE); // Espera semE ser liberado para executar por procE
    
    int seg = 1 + (rand()%4);
    printf("Iniciando E: %ds\n", seg);
    sleep(seg);
    printf("Finalizando processo E\n");
}

int main() {
	pthread_t thread[NUM_THREADS];
    void* procs[5] = {procA, procB, procC, procD, procE};
	
    // Inicia todos os semáforos como bloqueados
    sem_init(&semC1, 0, 0);
    sem_init(&semC2, 0, 0);
    sem_init(&semD, 0, 0);
    sem_init(&semE, 0, 0);

    srand(time(NULL));
    for(int i = 0; i < NUM_THREADS; i++) 
        pthread_create(&thread[i], NULL, procs[i], (void*)(intptr_t)i);
    
    for(int i=0; i<NUM_THREADS; i++)
		pthread_join (thread[i], NULL);
	
    exit(0);
}



