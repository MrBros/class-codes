# Trabalho 05 - Grafo de Precedência
Considere o seguinte grafo de precedência:

![Graph](/Sistemas Operacionais/assets/t05/graph.png "Graph")

Que será executado por três processos, conforme código abaixo:

P1: begin A; E; F; end;

P2: begin B; C; D; end;

P3: begin G; H; end;

Adicione semáforos a este programa, e as respec vas chamadas às suas operações, de
modo que a precedência definida acima seja alcançada.
Obdeça as equações obtendo valor final de
u dado as entradas de x , y e z

## Considerações sobre o trabalho
O entendimento do trabalho fica mais fácil se entender os processos como vértices do grafo e os semáforos como arestas.     

As funções foram definidas como P1, P2 e P3, cada uma sendo executada por uma thread.
* P1 contém procA, procE e procF;
* P2 contém procB, procC e procD;
* P3 contém procG e procH.
 