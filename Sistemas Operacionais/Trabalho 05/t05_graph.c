// gcc t05_graph.c -o t05_graph -lpthread && ./t05_graph

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>
#include <unistd.h>

#define NUM_THREADS 3

float x=0, y=0, z=0, h=0, j=0, u=0;
sem_t semB, semE, semG, semC, semD1, semD2, semH1, semH2, semF1, semF2;

void* P1(void* id) {
    float y_prev;
    
    // procA
    printf("Digite o valor de x: "); scanf("%f", &x);
    printf("Digite o valor de y: "); scanf("%f", &y);
    printf("Digite o valor de z: "); scanf("%f", &z);
    
    printf("\nP1\t:\tprocA\t:\tx = %.2f y = %.2f z = %.2f\n", x, y, z);

    sem_post(&semB); // Libera procB para executar
    sem_post(&semE); // Libera procE para executar
    sem_post(&semG); // Libera procG para executar

    // procE
    sem_wait(&semE); // Espera semE ser liberado para executar pelo procA
    
    y_prev = y;
    y += 2;
    
    printf("\nP1\t:\tprocE\t:\ty(%.2f) = y(%.2f) + 2 = %.2f\n", y_prev, y_prev, y);
    sem_post(&semD2); // Libera semD2 do procD para executar
    sem_post(&semH1); // Libera semH1 do procH para executar

    // procF
    sem_wait(&semF1); // Espera semF1 ser liberado para executar pelo procD
    sem_wait(&semF2); // Espera semF2 ser liberado para executar pelo procH

    u = h + j/3;
    printf("\nP1\t:\tprocF\t:\tu = h(%.2f) + j(%.2f)/3 = %.2f\n", h, j, u);
}

void* P2(void* id) {
    float x_prev;
    
    // procB
    sem_wait(&semB); // Espera semB ser liberado para executar pelo procA
    
    x_prev = x;
    x *= 2;
    printf("\nP2\t:\tprocB\t:\tx(%.2f) = x(%.2f) * 2 = %.2f\n", x_prev, x_prev, x);
    
    sem_post(&semC); // Libera procC para executar

    // procC
    sem_wait(&semC); // Espera semC ser liberado para executar pelo procB
    
    x_prev = x;
    x += 1;
    printf("\nP2\t:\tprocC\t:\tx(%.2f) = x(%.2f) + 1 = %.2f\n", x_prev, x_prev, x);
    
    sem_post(&semD1); // Libera semD1 do procD para executar

    // procD
    sem_wait(&semD1); // Espera semD1 ser liberado para executar pelo procC
    sem_wait(&semD2); // Espera semD2 ser liberado para executar pelo procE
    
    h = y + x;
    printf("\nP2\t:\tprocD\t:\th = y(%.2f) + x(%.2f) = %.2f\n", y, x, h);
    
    sem_post(&semF1); // Libera semF1 do procF para executar
}

void* P3(void* id) {
    float z_prev;
    
    // procG
    sem_wait(&semG); // Espera semG ser liberado para executar pelo procA

    z_prev = z;
    z /= 2;
    printf("\nP3\t:\tprocG\t:\tz(%.2f) = z(%.2f) / 2 = %.2f\n", z_prev, z_prev, z);

    sem_post(&semH2); // Libera semH2 do procH para executar

    // procH
    sem_wait(&semH1); // Espera semH1 ser liberado para executar pelo procE
    sem_wait(&semH2); // Espera semH2 ser liberado para executar pelo procG

    j = z + y - 4;
    printf("\nP3\t:\tprocH\t:\tj = z(%.2f) + y(%.2f) - 4 = %.2f\n", z, y, j);

    sem_post(&semF2); // Libera semF2 do procF para executar
}

int main() {
    pthread_t thread[NUM_THREADS];
    void* procs[NUM_THREADS] = {P1, P2, P3};

    // Inicia todos os semáforos como bloqueados
    sem_init(&semB, 0, 0);
    sem_init(&semC, 0, 0);
    sem_init(&semE, 0, 0);
    sem_init(&semG, 0, 0);
    sem_init(&semD1, 0, 0);
    sem_init(&semD2, 0, 0);
    sem_init(&semH1, 0, 0);
    sem_init(&semH2, 0, 0);
    sem_init(&semF1, 0, 0);
    sem_init(&semF2, 0, 0);

    srand(time(NULL));
    for(int i = 0; i < NUM_THREADS; i++) 
        pthread_create(&thread[i], NULL, procs[i], (void*)(intptr_t)i);
    
    for(int i=0; i<NUM_THREADS; i++)
		pthread_join (thread[i], NULL);

    printf("\nu = %.2f\n", u);
	
    exit(0);
}



