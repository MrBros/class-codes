# Trabalho 06 - Jantar de Gauleses - Threads
Uma tribo gaulesa janta em comunidade a partir de uma mesa enorme com espaço para
M javalis grelhados. Quando um gaulês quer comer, serve-se e retira um javali da mesa
a menos que esta já esteja vazia. Nesse caso o gaulês acorda o cozinheiro e aguarda que
este reponha javalis na mesa. O código seguinte representa o código que implementa o
gaulês e o cozinheiro.

![Code](/Sistemas Operacionais/assets/t06/code.png "Code")

Implemente o código das funções RetiraJavali() e ColocaJavalis() incluindo
código de sincronização que previna deadlock e acorde o cozinheiro apenas quando a
mesa está vazia.

Lembre que existem muitos gauleses e apenas um cozinheiro.

Identifique regiões críticas na vida do gaules e do cozinheiro.
A solução deve aceitar um numero N de gauleses igual ao número de letras de seu
primeiro nome e 1 único cozinheiro.
Cada gaules terá um nome, dado pela letra correspondente

> Ex: dalcimar = 8 gauleses

Cada gaules deve imprimir na tela seu nome (dado pela letra) quando come e
quando acorda o cozinheiro.

> Ex: Gaules d(0) comendo

> Ex: Gaules a(1) acordou cozinheiro

A quantidade javalis grelhados M deve ser igual ao número dos dois primeiros
dígitos de seu RA
A solução não deve ter nenhum comentário


## Considerações sobre o trabalho
Esse trabalho foi implementado utilizando threads, portanto cada gaulês é uma thread, bem como o cozinheiro.

O código foi feito seguindo o seguinte diagrama:

![Diagram](/Sistemas Operacionais/assets/t06/diagram.png "Diagram")

Onde:

1. O cozinheiro enche o caldeirão, entrega o sal aos gauleses e volta a dormir;
2. Um gaulês pega o sal, retira um javali e libera o sal para o próximo. Essa sequência se repete até o caldeirão ficar vazio;
3. O gaulês acorda o cozinheiro sinalizando que o caldeirão está vazio e espera o cozinheiro enchê-lo.
 