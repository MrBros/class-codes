// gcc t06_threads.c -o t06_threads -lrt -lpthread && ./t06_threads

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>
#include <unistd.h>

#define ever ;;
#define NUM_THREADS 6
#define M 19

int caldeirao = 0;
char *thread_names[NUM_THREADS-1] = {'m', 'a', 'r', 'i', 'o'};
sem_t sal, vazio;

void ColocaJavalis() { 
    caldeirao = M;
    printf("cozinheiro encheu o caldeirão\n");
    sleep(1);
}

void RetiraJavalis(void *id) { 
    long tid = (long)id;
    if(caldeirao == 0) {
        printf("Gaules %s(%ld) acordou cozinheiro\n", thread_names[tid], tid);
        sleep(1);
        sem_post(&vazio);
        sem_wait(&sal);
    }
    caldeirao--; 
}

void ComeJavali(void *id) {
    long tid = (long)id;
    printf("Gaules %s(%ld) comendo\n", thread_names[tid], tid);
}

void *Gaules(void* id) {
    long tid = (long)id;
    for(ever) {
        sem_wait(&sal);
        RetiraJavalis(id);
        sleep(1);
        sem_post(&sal); 
        ComeJavali(id);
    }
}

void *Cozinheiro() {
    for(ever) {
        sem_wait(&vazio);
        ColocaJavalis();
        sem_post(&sal);
        printf("cozinheiro voltou a dormir\n");
    }    
}


int main() {
    pthread_t thread[NUM_THREADS];

    sem_init(&sal, 0, 0);
    sem_init(&vazio, 0, 1);

    for(int i = 0; i < NUM_THREADS; i++) 
        if(i == NUM_THREADS - 1)
            pthread_create(&thread[i], NULL, Cozinheiro, (void*)(intptr_t)i);
        else
            pthread_create(&thread[i], NULL, Gaules, (void*)(intptr_t)i);
    
    for(int i=0; i<NUM_THREADS; i++)
		pthread_join (thread[i], NULL);

    exit(0);
}



