# Trabalho 7 - Jantar de Gauleses - Shared Memory
Implemente o problema do Jantar de Gauleses usando shared memory.
Pode usar funções da shmem (system V) ou mmap (POSIX), escolha livre.
Recomendo padrão POSIX.
Deve, obrigatóriamente ter um executável, código fonte separado para o produtor e outro para o consumidor.
Os programas podem ser lançados em background (&) ou utilizando fork/exec

## Considerações sobre o trabalho
O trabalho foi implementado conforme solicitado, separando um código fonte para o cozinheiro e outro para os gauleses. Os gauleses foram implementados utilizando threads. Para executar os programas em background, use:

```gcc -o cozinheiro cozinheiro.c -lrt -lpthread && gcc -o gauleses gauleses.c -lrt -lpthread```

```./cozinheiro & ./gauleses``` ou ```./gauleses & ./cozinheiro```

Assim como o [Trabalho 06](https://gitlab.com/MrBros/class-codes/-/tree/master/Sistemas%20Operacionais/Trabalho%2006), o código foi feito seguindo o seguinte diagrama:

![Diagram](/Sistemas Operacionais/assets/t06/diagram.png "Diagram")

Onde:

1. O cozinheiro enche o caldeirão, entregando o sal aos gauleses e sinalizando que o caldeirão não está mais vazio. O cozinheiro volta a dormir;
2. Um gaulês pega o sal, retira um javali e libera o sal para o próximo. Essa sequência se repete até o caldeirão ficar vazio;
3. O gaulês acorda o cozinheiro sinalizando que o caldeirão está vazio e espera o cozinheiro enchê-lo.
 
Mesmo utilizando shared memory, o diagrama fica igual. A única diferença é que agora a *struct caldeirao_t* fica sempre disponível em uma memória compartilhada, onde os dois processos podem acessá-lá tanto para leitura, quanto para escrita.