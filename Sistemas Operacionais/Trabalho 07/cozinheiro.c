// gcc -o cozinheiro cozinheiro.c -lrt -lpthread

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <semaphore.h>

#define SHRDNAME "/shared_memory"
#define ever ;;

typedef struct _caldeirao_t {
	sem_t sal, vazio;
	int num_javalis;
	int capacidade;
} caldeirao_t;

caldeirao_t *caldeirao;

void init_shared_memory() {
    int fd = shm_open(SHRDNAME, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);
	ftruncate(fd, sizeof(caldeirao_t));
	caldeirao = mmap(NULL, sizeof(caldeirao_t), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
}

void init_caldeirao() {
    sem_init(&(caldeirao->sal), 1, 0);
    sem_init(&(caldeirao->vazio), 1, 1);
	caldeirao->num_javalis = 0;
	caldeirao->capacidade = 19;
}

void ColocaJavalis(caldeirao_t *caldeirao) { 
    caldeirao->num_javalis = caldeirao->capacidade;
    printf("cozinheiro encheu o caldeirão com %2d javalis\n", caldeirao->num_javalis);
}

void *Cozinheiro() {
	for(ever) {
		sem_wait(&(caldeirao->vazio));
		ColocaJavalis(caldeirao);
		sem_post(&(caldeirao->sal));
		printf("cozinheiro voltou a dormir\n"); 
    } 
}

int main() {
	init_shared_memory();
	init_caldeirao();

	Cozinheiro();
}
