// gcc -o gauleses gauleses.c -lrt -lpthread

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>

#define SHRDNAME "/shared_memory"
#define NUM_THREADS 5
#define ever ;;

typedef struct _caldeirao_t {
	sem_t sal, vazio;
	int num_javalis;
	int capacidade;
} caldeirao_t;

char nomes[NUM_THREADS] = "mario";
caldeirao_t *caldeirao;

void init_shared_memory() {
    int fd = shm_open(SHRDNAME, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);
	ftruncate(fd, sizeof(caldeirao_t));
	caldeirao = mmap(NULL, sizeof(caldeirao_t), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
}

void RetiraJavalis(long tid) { 
    if(caldeirao->num_javalis == 0) {
        printf("Gaules %c(%ld) acordou cozinheiro\n", nomes[tid], tid);
        sem_post(&(caldeirao->vazio));
        sem_wait(&(caldeirao->sal));
    }
    caldeirao->num_javalis--; 
}

void ComeJavali(long tid) {
    printf("Gaules %c(%ld) comendo\n", nomes[tid], tid);
}

void *Gaules(void *id) {
    long tid = (long)id;
    for(ever) {
        sem_wait(&(caldeirao->sal));
        RetiraJavalis(tid);
        ComeJavali(tid);
        sem_post(&(caldeirao->sal));    
        sleep(1);
    }
}

int main() {
    init_shared_memory();
    
    pthread_t gaules[NUM_THREADS];
	for(int i = 0; i < NUM_THREADS; i++) 
        pthread_create(&gaules[i], NULL, Gaules, (void*)(intptr_t)i);
    for(long i = 0; i < NUM_THREADS; i++)
        pthread_join(gaules[i], NULL);

    pthread_exit(NULL);
}
