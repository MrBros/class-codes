# Trabalho 8 - Jantar de Gauleses - Pipes
Implemente o problema do Jantar de Gauleses usando pipes.

Usar pipes ou named pipes.

Deve, obrigatóriamente ter um executável, código fonte separado para o produtor e outro para o consumidor.

Os programas podem ser lançados utilizando fork/exec.

Controlar o tamanho do buffer usando pipes é problemá co, já que o pipe tem um tamanho fixo dado pelo sistema operacional. 

Sugestões de solução para 'emular' um buffer usando pipes:
* Não fazer nada (terá um pequeno desconto na loja);
* Descobrir o tamanho do pipe e mandar dados de tamanho correto. Ex: pipe 100kb total, buffer de 5 elementos, cada ‘write’ ou ‘read’ no pipe feito com 20kb (100/5);
* Utilizar um esquema de passagem de mensagens (consumidor envia 5 mensagens vazias para produtor);

Desenvolva uma estratégia para comunicar 1 produtor (cozinheiro) e N consumidores (gauleses). 

Sugestão: utilizar threads no processo gaules.

## Considerações sobre o trabalho
O trabalho foi implementado conforme solicitado, separando um código fonte para o cozinheiro e outro para os gauleses. Os gauleses foram implementados utilizando threads. Para executar os programas em background, use:

```gcc -o cozinheiro cozinheiro.c -lrt -lpthread && gcc -o gauleses gauleses.c -lrt -lpthread```

```./cozinheiro & ./gauleses``` ou ```./gauleses & ./cozinheiro```

> Utilizei a execução em background apenas por comodidade. Para executar utilizando fork/exec, será necessário criar outro arquivo '.c' para reunir os outros dois processos. Caso esteja em dúvida sobre como utilizar fork(), utilize o [Trabalho 01](https://gitlab.com/MrBros/class-codes/-/tree/master/Sistemas%20Operacionais/Trabalho%2001) como exemplo.

Utilizando como base o diagrama dos trabalhos [06](https://gitlab.com/MrBros/class-codes/-/tree/master/Sistemas%20Operacionais/Trabalho%2006) e [07](https://gitlab.com/MrBros/class-codes/-/tree/master/Sistemas%20Operacionais/Trabalho%2007), obtemos o seguinte diagrama:

![Diagram](/Sistemas Operacionais/assets/t08/diagram.png "Diagram")

Onde:

1. O cozinheiro enche o caldeirão, entregando o sal aos gauleses e sinalizando que o caldeirão não está mais vazio. O cozinheiro volta a dormir;
2. Um gaulês pega o sal, retira um javali e libera o sal para o próximo. Essa sequência se repete até o caldeirão ficar vazio;
3. O gaulês acorda o cozinheiro sinalizando que o caldeirão está vazio e espera o cozinheiro enchê-lo.

*caldeirao_vazio*: *pipe* que enviará a *struct caldeirao_t* do processo *gauleses* para o processo *cozinheiro* com as variáveis devidamente configuradas para que o cozinheiro possa encher o caldeirão. 

*caldeirao_cheio*: *pipe* que enviará a *struct caldeirao_t* do processo *cozinheiros* para o processo *gauleses* com as variáveis devidamente configuradas para que os gauleses possam retirar os javalis do caldeirão.

Diferente dos outros trabalhos, os dados não ficam sempre disponíveis para leitura e/ou escrita dos processos. Portanto é necessário enviar a *struct caldeirao_t* pelo *pipe* de saída correspondente quando a mesma é alterada pelo processo. No caso do cozinheiro, a *struct caldeirao_t* é enviada quando o cozinheiro enche o caldeirao e no caso dos gauleses, quando o caldeirão é esvaziado.