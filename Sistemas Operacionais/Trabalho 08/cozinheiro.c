// gcc -g -o cozinheiro cozinheiro.c -lrt -lpthread
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <semaphore.h>

#define ever ;;

typedef struct _calderao_t {
	sem_t sal, vazio;
	int num_javalis;
	int capacidade;
} caldeirao_t;

caldeirao_t *caldeirao;
int fd_entrada, fd_saida;

void init_pipe_entrada() {
    mkfifo("caldeirao_vazio", 0666);
    fd_entrada = open("caldeirao_vazio", O_RDONLY);
}

void init_pipe_saida() {
    mkfifo("caldeirao_cheio", 0666);
    fd_saida = open("caldeirao_cheio", O_WRONLY);
}
void init_caldeirao() {
    caldeirao = malloc(sizeof(caldeirao_t));
    sem_init(&(caldeirao->sal), 0, 0);
    sem_init(&(caldeirao->vazio), 0, 1);
	caldeirao->num_javalis = 0;
	caldeirao->capacidade = 19;
}

void ColocaJavalis() { 
    caldeirao->num_javalis = caldeirao->capacidade;
    printf("cozinheiro encheu o caldeirão com %2d javalis\n", caldeirao->num_javalis);
}

void *Cozinheiro() {
    for(ever) {
        read(fd_entrada, caldeirao, sizeof(caldeirao_t)); 
		sem_wait(&(caldeirao->vazio));
		ColocaJavalis();
		sem_post(&(caldeirao->sal));
        write(fd_saida, caldeirao, sizeof(caldeirao_t));
		printf("cozinheiro voltou a dormir\n"); 
    } 
}

int main() {
	init_pipe_saida();
    init_pipe_entrada();
	init_caldeirao();

	Cozinheiro();
    
    free(caldeirao);
}
