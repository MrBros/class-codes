// gcc -g -o gauleses gauleses.c -lrt -lpthread
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>

#define NUM_THREADS 5
#define ever ;;

typedef struct _caldeirao_t {
	sem_t sal, vazio;
	int num_javalis;
	int capacidade;
} caldeirao_t;

char nomes[NUM_THREADS] = "mario";
caldeirao_t *caldeirao;
int fd_entrada, fd_saida;

void init_pipe_entrada() {
    mkfifo("caldeirao_cheio", 0666);
    fd_entrada = open("caldeirao_cheio", O_RDONLY);
}

void init_pipe_saida() {
    mkfifo("caldeirao_vazio", 0666);
    fd_saida = open("caldeirao_vazio", O_WRONLY);
}

void init_caldeirao() {
    caldeirao = malloc(sizeof(caldeirao_t));
    sem_init(&(caldeirao->sal), 1, 0);
    sem_init(&(caldeirao->vazio), 1, 1);
	caldeirao->num_javalis = 0;
	caldeirao->capacidade = 19;
}

void RetiraJavalis(long tid) { 
    if(caldeirao->num_javalis == 0) {
        printf("Gaules %c(%ld) acordou cozinheiro\n", nomes[tid], tid);
        sem_post(&(caldeirao->vazio));
        write(fd_saida, caldeirao, sizeof(caldeirao_t));
        read(fd_entrada, caldeirao, sizeof(caldeirao_t));
        sem_wait(&(caldeirao->sal));
    }
    caldeirao->num_javalis--; 
}

void ComeJavali(long tid) {
    printf("Gaules %c(%ld) comendo, restam %2d javalis\n", nomes[tid], tid, caldeirao->num_javalis);
}

void *Gaules(void *id) {
    long tid = (long)id;
    for(ever) {
        sem_wait(&(caldeirao->sal));
        RetiraJavalis(tid);
        ComeJavali(tid);
        sem_post(&(caldeirao->sal));    
        sleep(1);
    }
}

int main() {
    init_pipe_entrada();
    init_pipe_saida();
    init_caldeirao();
    
    write(fd_saida, caldeirao, sizeof(caldeirao_t));
    read(fd_entrada, caldeirao, sizeof(caldeirao_t));

    pthread_t gaules[NUM_THREADS];
	for(int i = 0; i < NUM_THREADS; i++) 
        pthread_create(&gaules[i], NULL, Gaules, (void*)(intptr_t)i);
    for(long i = 0; i < NUM_THREADS; i++)
        pthread_join(gaules[i], NULL);

    free(caldeirao);
    pthread_exit(NULL);
}
