# Trabalho 12 - Jantar de Gauleses - Passagem de Mensagem
Implemente o problema do Jantar de Gauleses usando MPI (família mpi_send) ou POSIX
(famílio mk_send) escolha livre.

Recomendo padrão POSIX, pois foi a ensinada nos vídeos e está nativamente instalada. Para quem quiser usar MPI tem slides, e vc irá precisar instalar o pacote.

Deve, obrigatóriamente ter um único executável, código fonte o produtor e para o
consumidor no mesmo arquivo

## Considerações sobre o trabalho
O trabalho foi implementado conforme solicitado, separando um código fonte para o cozinheiro e outro para os gauleses. Os gauleses foram implementados utilizando threads. Para executar os programas em background, use:

```gcc -o cozinheiro cozinheiro.c -lrt -lpthread && gcc -o gauleses gauleses.c -lrt -lpthread```

```./cozinheiro & ./gauleses``` ou ```./gauleses & ./cozinheiro```

> Fiz o trabalho em 2 arquivos por comodidade. Para fazer em um arquivo apenas,  deve-se levar em conta que *fd_in* e *fd_out* do arquivo *cozinheiro.c* são, respectivamente, *fd_out* e *fd_in* de *gauleses.c*. Deve-se lembrar também de transformar o cozinheiro em uma *thread*, assim como o [Trabalho 06](https://gitlab.com/MrBros/class-codes/-/tree/master/Sistemas%20Operacionais/Trabalho%2006).

Com base nos trabalhos anteriores, obtemos o seguinte diagrama: 

![Diagram](/Sistemas Operacionais/assets/t12/diagram.png "Diagram")

Onde:

1. O cozinheiro enche o caldeirão, entregando o sal aos gauleses e sinalizando que o caldeirão não está mais vazio. O cozinheiro volta a dormir;
2. Um gaulês pega o sal, retira um javali e libera o sal para o próximo. Essa sequência se repete até o caldeirão ficar vazio;
3. O gaulês acorda o cozinheiro sinalizando que o caldeirão está vazio e espera o cozinheiro enchê-lo.

*caldeirao_vazio*: *message_queue* que enviará a *struct caldeirao_t* do processo *gauleses* para o processo *cozinheiro* com as variáveis devidamente configuradas para que o cozinheiro possa encher o caldeirão. 

*caldeirao_cheio*: *message_queue* que enviará a *struct caldeirao_t* do processo *cozinheiros* para o processo *gauleses* com as variáveis devidamente configuradas para que os gauleses possam retirar os javalis do caldeirão.

Diferente dos outros trabalhos, os dados não ficam sempre disponíveis para leitura e/ou escrita dos processos. Portanto é necessário enviar a *struct caldeirao_t* pela *message_queue* de saída correspondente quando a mesma é alterada pelo processo. No caso do cozinheiro, a *struct caldeirao_t* é enviada quando o cozinheiro enche o caldeirao e no caso dos gauleses, quando o caldeirão é esvaziado.