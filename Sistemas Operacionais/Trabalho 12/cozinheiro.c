// gcc -g -o cozinheiro cozinheiro.c -lrt -lpthread

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <mqueue.h>
#include <unistd.h>
#include <semaphore.h>
#include <time.h>

#define ever ;;

struct mq_attr attr;
typedef struct _calderao_t {
	sem_t sal, vazio;
	int num_javalis;
	int capacidade;
} caldeirao_t;

caldeirao_t *caldeirao;
mqd_t fd_in, fd_out;

void init_mesa_in() {
    mq_unlink("/caldeirao_vazio");
    fd_in = mq_open("/caldeirao_vazio", O_RDWR|O_CREAT, 0666, &attr);
}

void init_mesa_out() {
    mq_unlink("/caldeirao_cheio");
	fd_out = mq_open("/caldeirao_cheio", O_RDWR|O_CREAT, 0666, &attr);
}

void init_caldeirao() {
    caldeirao = malloc(sizeof(caldeirao_t));
    sem_init(&(caldeirao->sal), 1, 0);
    sem_init(&(caldeirao->vazio), 1, 1);
	caldeirao->num_javalis = 0;
	caldeirao->capacidade = 19;
}

void ColocaJavalis() { 
    caldeirao->num_javalis = caldeirao->capacidade;
    printf("cozinheiro encheu o caldeirão com %2d javalis\n", caldeirao->num_javalis);
}

void Cozinheiro() {
    for(ever) {
        mq_receive(fd_in, (void *)caldeirao, sizeof(caldeirao_t), 0); 
		sem_wait(&(caldeirao->vazio));
		ColocaJavalis();
		sem_post(&(caldeirao->sal));
        mq_send(fd_out, (void *)caldeirao, sizeof(caldeirao_t), 0);
		printf("cozinheiro voltou a dormir\n"); 
    } 
}

int main() {
    attr.mq_maxmsg = 1;
	attr.mq_msgsize = sizeof(caldeirao_t);
	attr.mq_flags = 0;
    
    init_caldeirao();
    init_mesa_in();
    init_mesa_out();

    Cozinheiro();

    free(caldeirao);
}
