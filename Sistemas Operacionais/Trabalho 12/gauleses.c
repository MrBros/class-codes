// gcc -g -o cozinheiro cozinheiro.c -lrt -lpthread

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <mqueue.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>

#define NUM_THREADS 5
#define ever ;;

struct mq_attr attr;
typedef struct _caldeirao_t {
	sem_t sal, vazio;
	int num_javalis;
	int capacidade;
} caldeirao_t;

char nomes[NUM_THREADS] = "mario";
caldeirao_t *caldeirao;
mqd_t fd_in, fd_out;

void init_mesa_in() {
    fd_in = mq_open("/caldeirao_cheio", O_RDWR);
}

void init_mesa_out() {
    fd_out = mq_open("/caldeirao_vazio", O_RDWR);
}

void init_caldeirao() {
    caldeirao = malloc(sizeof(caldeirao_t));
    sem_init(&(caldeirao->sal), 1, 0);
    sem_init(&(caldeirao->vazio), 1, 1);
	caldeirao->num_javalis = 0;
	caldeirao->capacidade = 19;
}

void RetiraJavalis(long tid) { 
    if(caldeirao->num_javalis == 0) {
        printf("Gaules %c(%ld) acordou cozinheiro\n", nomes[tid], tid);
        sem_post(&(caldeirao->vazio));
        mq_send(fd_out, (void *)caldeirao, sizeof(caldeirao_t), 0);
        mq_receive(fd_in, (void *)caldeirao, sizeof(caldeirao_t), 0);
        sem_wait(&(caldeirao->sal));
    }
    caldeirao->num_javalis--; 
}

void ComeJavali(long tid) {
    printf("Gaules %c(%ld) comendo, restam %2d javalis\n", nomes[tid], tid, caldeirao->num_javalis);
}

void *Gaules(void *id) {
    long tid = (long)id;
    for(ever) {
        sem_wait(&(caldeirao->sal));
        RetiraJavalis(tid);
        ComeJavali(tid);
        sem_post(&(caldeirao->sal));    
        sleep(1);
    }
}

int main() {
    attr.mq_maxmsg = 1;
	attr.mq_msgsize = sizeof(caldeirao_t);
	attr.mq_flags = 0;

    init_caldeirao();
    init_mesa_in();
    init_mesa_out();
   
    mq_send(fd_out, (void *)caldeirao, sizeof(caldeirao_t), 0);
    mq_receive(fd_in, (void *)caldeirao, sizeof(caldeirao_t), 0);

    pthread_t gaules[NUM_THREADS];
	for(int i = 0; i < NUM_THREADS; i++) 
        pthread_create(&gaules[i], NULL, Gaules, (void*)(intptr_t)i);
    for(long i = 0; i < NUM_THREADS; i++)
        pthread_join(gaules[i], NULL);

    free(caldeirao);
    pthread_exit(NULL);
}
